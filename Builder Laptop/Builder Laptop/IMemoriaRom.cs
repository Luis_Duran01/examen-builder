﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    interface IMemoriaRom
    {
        string propiedad();
    }
    class MemoriaRomBasica : IMemoriaRom
    {
        public string propiedad()
        {
            return "Memoria de almacenamiento de 250gb";
        }
    }
    class MemoriaRomMediana : IMemoriaRom
    {
        public string propiedad()
        {
            return "Memoria de almacenamiento de 500gb";
        }
    }
    class MemoriaRomAvanzada: IMemoriaRom
    {
        public string propiedad()
        {
            return "Memoria de almacenamiento de 1tb";
        }
    }
}
