﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    interface IGrafica
    {
        string singularidad();
    }
    class GraficaBasica : IGrafica
    {
        public string singularidad()
        {
            return "Grafica Integraqda";
        }
    }
    class GraficaMediana : IGrafica
    {
        public string singularidad()
        {
            return "Grafica NVDIA 1050";
        }
    }
    class GrafifcaAvanzada : IGrafica
    {
        public string singularidad()
        {
            return "Grafica NVDIA 1050Ti";
        }
    }

}
