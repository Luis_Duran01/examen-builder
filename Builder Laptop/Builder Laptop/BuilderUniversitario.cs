﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    class BuilderUniversitario : IBuilder
    {
        private Producto laptop = new Producto();
        public void ConstruyeProcesador()
        {
            laptop.ColocarProcesador(new ProcesadorMediano());
        }
        public void ConstrlleMemoriaRam()
        {
            laptop.ColocarMemoriaRam(new MemoriaRamMediana());
        }
        public void ConstrulleMemoriaRom()
        {
            laptop.ColocarMemoriaRom(new MemoriaRomMediana());
        }
        public void ConstrulleGrafica()
        {
            laptop.ColocarGracfica(new GraficaMediana());
        }
        public Producto ObtenProducto()
        {
            return laptop;
        }
    }
}
