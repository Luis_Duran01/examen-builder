﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    interface IProcesador
    {
        string especificaciones();
    }
    class ProcesadorBasico : IProcesador
    {
        public string especificaciones()
        {
            return "Procesador core i3";
        }
    }
    class ProcesadorMediano : IProcesador
    {
        public string especificaciones()
        {
            return "Procesadsor core i5";
        }

    }
    class ProcesadorAvanzado : IProcesador
    {
        public string especificaciones()
        {
            return "Procesador core i7";
        }
    }

}
