﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    interface IMemoriaRam
    {
        string caracteristicas();
    }
    class MemoriaRamBasica : IMemoriaRam
    {
        public string caracteristicas()
        {
            return "Memoria Ram 4gb";
        }
    }
    class MemoriaRamMediana : IMemoriaRam
    {
        public string caracteristicas()
        {
            return "Memoria Ram 8gb";
        }
    }
    class MemoriaRamAvanzada : IMemoriaRam
    {
        public string caracteristicas()
        {
            return "Memoria Ram 16gb";
        }
    }
}
