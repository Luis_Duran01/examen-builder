﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    class BuilderGamer : IBuilder
    {
        private Producto laptop = new Producto();
        public void ConstrulleProcesador()
        {
            laptop.ColocarProcesador(new ProcesadorAvanzado());
        }
        public void ConstrulleMemoriaRam()
        {
            laptop.ColocarMemoriaRam(new MemoriaRamAvanzada());
        }
        public void ConstrulleMemoriaRom()
        {
            laptop.ColocarMemoriaRom(new MemoriaRomAvanzada());
        }
        public void ConstrulleGrafica()
        {
            laptop.ColocarGracfica(new GrafifcaAvanzada());
        }
        public Producto ObtenProducto()
        {
            return laptop;
        }
    }
}
