﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    class Director
    {
        public void construye(IBuilder pConstructor)
        {
            //Aqui el Director indica los pasos para hacer la construccion
            //pero el contructor es el que se encarga de construir segun 
            //esta especificaciones 

            pConstructor.ConstrulleProcesador();
            pConstructor.ConstrulleMemoriaRam();
            pConstructor.ConstrulleMemoriaRom();
            pConstructor.ConstrulleGrafica();
        }
    }
}
