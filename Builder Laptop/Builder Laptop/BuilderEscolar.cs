﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    class BuilderEscolar : IBuilder
    {
        private Producto laptop = new Producto();
        public void ConstrulleProcesador()
        {
            laptop.ColocarProcesador(new ProcesadorBasico());
        }
        public void ConstrulleMemoriaRam()
        {
            laptop.ColocarMemoriaRam(new MemoriaRamBasica());
        }
        public void ConstrulleMemoriaRom()
        {
            laptop.ColocarMemoriaRom(new MemoriaRomBasica());
        }
        public void ConstrulleGrafica()
        {
            laptop.ColocarGracfica(new GraficaBasica());
        }
        public Producto ObtenProducto()
        {
            return laptop;
        }

    }
}
