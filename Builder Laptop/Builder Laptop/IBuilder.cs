﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    interface IBuilder
    {
        void ConstrulleProcesador();
        void ConstrulleMemoriaRam();
        void ConstrulleMemoriaRom();
        void ConstrulleGrafica();
    }
}
