﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder_Laptop
{
    class Producto
    {
        private IProcesador procesador;
        private IMemoriaRam memoriaRam;
        private IMemoriaRom memoriaRom;
        private IGrafica grafica;

        public void ColocarProcesador(IProcesador pPocesador)
        {
            procesador = pPocesador;
            Console.WriteLine("Se ha colocado el procesador:{0}", procesador.especificaciones());
        }
        public void ColocarMemoriaRam(IMemoriaRam pMemoriaRam)
        {
            memoriaRam = pMemoriaRam;
            Console.WriteLine("Se ha colocado la memoria RAM :{0}", memoriaRam.caracteristicas());
        }
        public void ColocarMemoriaRom(IMemoriaRom pMemoriaRom)
        {
            memoriaRom = pMemoriaRom;
            Console.WriteLine("Se ha colocado el almacenamiento :{0}", memoriaRom.propiedad());
        }
        public void ColocarGracfica(IGrafica pGrafica)
        {
            grafica = pGrafica;
            Console.WriteLine("Se ha colocado la grafica :{0}", grafica.singularidad());
        }
        public void MostrarLaptop()
        {
            Console.WriteLine("Tu laptop tiene {1} {2} {3} {4}", procesador.especificaciones(), memoriaRom.propiedad(), memoriaRam.caracteristicas(), grafica.singularidad());
        }
    }
}
