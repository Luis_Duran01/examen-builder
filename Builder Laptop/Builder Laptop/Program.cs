﻿using System;

namespace Builder_Laptop
{
    class Program
    {
        static void Main(string[] args)
        {
            Director miDirector = new Director();


            //construimos una laptop economico
            BuilderEscolar escolar = new BuilderEscolar();
            miDirector.construye(escolar);

            //Obtenemos el auto
            Producto laptop1 = escolar.ObtenProducto();

            laptop1.MostrarLaptop();

            Console.WriteLine("------");

            ////construimos una laprotp Mediana 
            //BuilderUniversitario universitario = new BuilderUniversitario();
            //miDirector.construye(universitario);

            ////obtenemois la laptop2
            //Producto laptop2 = Universitario.ObtenProducto();
            //laptop2.MostrarLaptop();

            ////laptop avanzada 
            //BuilderGamer gamer = new BuilderGamer();
            //miDirector.construye(gamer);


            //// obtenemos laptop3
            //Producto laptop3 = gamer.ObtenProducto();
            //laptop3.MostrarLaptop();

        }
    }
}
